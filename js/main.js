const containerTickets = document.getElementById("containerTicket");

for (let i = 0; i < 150; i++) {
  const ticket = `
    <br>
    <div class="row decoTickets">
        <div class="col-12 border py-3 d-flex align-items-center justify-content-center flex-column"
            style="border-radius: 20px; border-bottom: none !important;">
            <div
                class="getQrCode d-flex align-items-center flex-column justify-content-center">
            </div>
        </div>
        <div class="col-12 py-5 contInfo" style="border-radius: 15px;">
            <div class="infoEvento">
                <strong>Presenta:</strong>
                <p class="m-0">@DjNinaCaballero</p>
                <small>Reggaeton, dancehall & electrónica</small>
                <hr>
                <strong>Nombre Evento: </strong>
                <p class="m-0">Noche De Entierro</p>
                <br>
                <strong>Artistas:</strong>
                <p class="m-0">Nina Caballero</p>
                <p class="m-0">Netologia</p>
                <p class="m-0">Wanlof</p>
                <p class="m-0">Miguel Olguin</p>
                <br>
                <strong>Horario: </strong>
                <p class="m-0"> 6 de Noviembre del 2020 - 8pm a 3am</p>
                <br>
                <strong>Ubicación: </strong>
                <p class="m-0">Locación Secreta</p>
                <br>
                <h5>Costo: 20.000 cop</h5>
                <p class="textInfo m-0">
                    Esta entrada es personal e intransferible, una vez sea
                    escaneada, quedara totalmente invalida.
                </p>
            </div>
        </div>
        <div class="col-12 border pb-4 d-flex"
            style="border-radius: 20px; border-top: none !important;">
            <!--
            
            <div class="infoSec">
                <p class="m-0">Powered By:</p>
                <strong>Mateus Tech SAS</strong>
            </div>
            
            -->
            
            <div class="infoContrato">
                <br>
                <br>
                <p class="numeroTiquete w-100 text-right m-0">Tiquete Numero: 000${i}</p>
                <hr>
                <p class="m-0 mt-2">Se reserva el derecho de admisión.</p>
                <p class="m-0 mt-2">Cumplimos con todos los protocolos de bioseguridad y protocolo exigidos por las autoridades </p>
                <p class="m-0 mt-2">Si necesitas boletas electrónicas por favor contactarse: <strong>316 698
                    9045</strong> </p>
            </div>
        </div>
    </div>
    <br>
    `;

  containerTickets.innerHTML += ticket;
}

setTimeout(() => {
  const imprimirQr = document.getElementsByClassName("getQrCode");
  for (let x = 0; x < imprimirQr.length; x++) {
    // url escaneo https://entrafacil.com/tickets/clave-nombreEvento-numeroDeTiquetesTotales/horaCreacion/numTicket/usuarioLector
    const password = `crooked13~NocheDeEntierro~150`;
    const time = new Date().getTime();
    const url = `https://entrafacil.com/tickets/${rot13(
      password
    )}/${time}/${x}/${rot13("DanielMateus")}`;
    const qrcode = new QRCode(imprimirQr[x], {
      text: url,
      width: 200,
      height: 200,
      colorDark: "#000000",
      colorLight: "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
    });
  }
}, 3000);

function rot13(s) {
  return (s ? s : this)
    .split("")
    .map(function (_) {
      if (!_.match(/[A-Za-z]/)) return _;
      c = Math.floor(_.charCodeAt(0) / 97);
      k = (_.toLowerCase().charCodeAt(0) - 83) % 26 || 26;
      return String.fromCharCode(k + (c == 0 ? 64 : 96));
    })
    .join("");
}

let x = 0;

setTimeout(()=>{
  setInterval(() => {
    console.log('conteo de interval : ' + x);
    const tomarSS = document.getElementsByClassName("decoTickets");
    html2canvas(tomarSS[x])
      .then((canvas) => {
        let enlace = document.createElement("a");
        enlace.download = `Tiquete No 00${x}`;
        enlace.href = canvas.toDataURL();
        enlace.click();
      })
      .catch((err) => {
        console.error(err);
      });
    x++;
  }, 2000);
}, 20000)
